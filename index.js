const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('./utils/redis');

const app = express()
app.use(bodyParser.json());

//specifying the listening port
app.listen(4000, () => {
    console.log('Listening on port 4000')
})

//===================================================================================================

mongoose.connect('mongodb://mongo', {
    useUnifiedTopology: true,
    useNewUrlParser: true
});

//===================================================================================================
const articleSchema = new mongoose.Schema({
    title: String,
    author: String,
    content: String,
    createdAt: {type: Date, default: Date.now},
});

mongoose.model('Article', articleSchema);

const Article = mongoose.model('Article');

//===================================================================================================
//defining the root endpoint
app.get('/api/articles', async (req, res) => {
    console.time('Article');
    const articles = await Article.find()
        .cache({expire: 10});
    console.timeEnd('Article');

    res.json(articles);
});

app.post('/api/articles', async (req, res) => {
    const {title, author, content} = req.body;

    if (!title || !author || !content) {
        return res.status(400).send('Missing title, author, or content')
    }

    const article = new Article({
        title,
        author,
        content
    });

    try {
        await article.save();
        res.send(article);
    } catch (err) {
        res.status(400).send(err.message);
    }
});

//===================================================================================================
